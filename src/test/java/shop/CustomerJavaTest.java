package cz.cvut.eshop.shop;

import org.junit.Assert;
import org.junit.Test;

public class CustomerJavaTest {

    @Test
    public void Customer_AddCustomerTest() {
        //setup
        Customer NewCustomer = new Customer(20);
        //assert
        Assert.assertEquals(20, NewCustomer.getLoyaltyPoints());
    }

    @Test
    public void Customer_AddLoyalityPointsTest() {
        //setup
        Customer NewCustomer = new Customer(20);
        NewCustomer.addLoayltyPoint(10);
        //asert
        Assert.assertEquals(30, NewCustomer.getLoyaltyPoints());
    }

    @Test
    public void CustomerTest_SetCustomerNameTest() {
        //setup
        Customer NewCustomer = new Customer(20);
        NewCustomer.setCustomerName("John");
        //asert
        Assert.assertEquals("John", NewCustomer.getCustomerName());
        //setup
        NewCustomer.setCustomerAddress("Baker street 221b");
        //asert
        Assert.assertEquals("Baker street 221b", NewCustomer.getCustomerAddress());
    }

}
