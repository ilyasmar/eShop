package cz.cvut.eshop.shop;

import org.junit.Assert;
import org.junit.Test;

public class ItemTest {

    @Test
    public void ItemSetTest(){
        Item FoodItem = new StandardItem(1, "Rice", 7.0f, "food", 5);
        Assert.assertEquals("Rice", FoodItem.getName());
        Assert.assertEquals(1, FoodItem.getID());
        FoodItem.setName("Potato");
        Assert.assertEquals("Potato", FoodItem.getName());
        FoodItem.setID(2);
        Assert.assertEquals(2, FoodItem.getID());
        FoodItem.setCategory("root-crop");
        Assert.assertEquals("root-crop", FoodItem.getCategory());
        Assert.assertNotEquals("Item ID 2   NAME Potato    CATEGORY root-crop", FoodItem.toString());
        FoodItem.setPrice(10.0f);
        Assert.assertEquals(10.0f, 1.0f, FoodItem.getPrice());

    }

    @Test
    public void EqualItemsTest_True() {
        Item FirstCoffee = new StandardItem(5, "Coffee", 23.0f, "food", 7);
        Item SecondCoffee = new StandardItem(5, "Coffee", 23.0f, "food", 7);
        Assert.assertTrue(FirstCoffee.equals(SecondCoffee));
    }

    @Test
    public void EqualItemTest_False(){
        Item FirstPotato = new StandardItem(1, "Potato", 7.0f, "food", 5);
        Item SecondPotato = new StandardItem(2, "Potato", 8.0f, "food", 5);
        Assert.assertFalse(FirstPotato.equals(SecondPotato));
    }

}
